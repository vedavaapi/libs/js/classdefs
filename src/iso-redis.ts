/**
 * Methods for connecting and operating over redis-server
 * These methods only works on node. on browsers, they didn't do any thing.
 * Mainly, this file doesn't declare redis as dependency, but require dynamically, and thus redis will not be get bundled in browser specific build.
 */

import { isBrowser } from './utils';


export function getRedisClient(options: any): any {
    let client;
    if (!isBrowser) {
        // eslint-disable-next-line no-eval
        client = eval('require')('redis').createClient(options); // NOTE used eval to escape redis from umd bundles, for isomorphic apps
    }
    return client;
}
