import { objstore, Context } from '@vedavaapi/client';
import { IProjection } from '@vedavaapi/client/dist/api/api-param-models';
import { IStore } from '@vedavaapi/iso-kvstores/dist/IStore';
import { IPlainObject } from '@vedavaapi/types/dist/base';
import Emitter from 'component-emitter';

import { getStore } from '../IsoStore';
import { MetaStore, IStoreMeta } from './meta-store';
import { ClassHierarchyStore } from './class-hierarchy-store';
import { entriesToMap, msToDate, compileAndSerializeUiProfileTemplates, deserializeUiProffileTemplates } from '../utils';
import { IUiProfile } from '../types';
import { UiCompiliedProfileTemplate } from '../models';


export interface IUiProfilesMap {
    [className: string]: IUiProfile;
}

export interface IUiCompiledProfilesMap {
    [className: string]: UiCompiliedProfileTemplate;
}

/**
 * Loads all existing profile names on initialization, and keeps them in sync with upstream with periodic refresh/ new-class-add events from chStore
 * And also lazily caches Profile objets, and manages, serves them.
 */
export class UiProfileStore extends Emitter {
    public static readonly storeType = 'UiProfile';

    private metaStore: MetaStore;

    private chStore: ClassHierarchyStore;

    private store: IStore

    private inMemorCompiledProfilesCache: IUiCompiledProfilesMap;

    private namesInvalidationInterval: number;

    private profilesInvalidationInterval: number;

    private profilesUpdateCheckInterval: number;

    constructor({ store, metaStore, chStore, namesInvalidationInterval, profilesInvalidationInterval, profilesUpdateCheckInterval }: {
        store: IStore,
        metaStore: MetaStore,
        chStore: ClassHierarchyStore,
        namesInvalidationInterval?: number,
        profilesInvalidationInterval?: number,
        profilesUpdateCheckInterval?: number,
    }) {
        super();
        this.store = store;
        this.metaStore = metaStore;
        this.chStore = chStore;
        this.inMemorCompiledProfilesCache = {};
        this.namesInvalidationInterval = namesInvalidationInterval || 86400 * 1000;
        this.profilesInvalidationInterval = profilesInvalidationInterval || 86400 * 1000 * 10;
        this.profilesUpdateCheckInterval = profilesUpdateCheckInterval || 86400 * 1000;
    }

    private static async fetchClassDefs(vc: Context, classNamesDomain?: string[], projection?: IProjection, customFilter?: IPlainObject) {
        const selectorDoc: IPlainObject = {
            jsonClass: 'VVSchema',
            _ui_profile: {
                $exists: true,
            },
        };
        if (classNamesDomain && classNamesDomain.length) {
            selectorDoc.name = { $in: classNamesDomain };
        }
        if (customFilter) Object.assign(selectorDoc, customFilter);
        const jsonClassProjectionMap: IPlainObject = {};
        if (projection) {
            jsonClassProjectionMap.VVSchema = projection;
        }

        return objstore.Graph.put({
            vc,
            data: {
                start_nodes_selector: selectorDoc,
                max_hops: 0,
                json_class_projection_map: jsonClassProjectionMap,
                direction: 'referrer',
            },
        });
    }

    private static async fetchExistingProfileNames(vc: Context, classNamesDomain?: string[]) {
        const { graph } = (await UiProfileStore.fetchClassDefs(vc, classNamesDomain, { name: 1 })).data;
        return Object.keys(graph).map((_id) => graph[_id].name);
    }

    private static async fetchUiProfiles(vc: Context, classNamesDomain?: string[], customFilter?: IPlainObject) {
        const { graph } = (await UiProfileStore.fetchClassDefs(vc, classNamesDomain, { _ui_profile: 1, name: 1 }, customFilter)).data;

        const uiProfilesMap: IUiProfilesMap = {};
        Object.keys(graph).forEach((_id) => {
            const classDef = graph[_id];
            uiProfilesMap[classDef.name] = compileAndSerializeUiProfileTemplates(classDef._ui_profile);
        });
        return uiProfilesMap;
    }

    public getStoreMeta() {
        return this.metaStore.get(UiProfileStore.storeType);
    }

    public getExistingProfileNames() {
        return (this.getStoreMeta() as IStoreMeta).existingProfileNames || [];
    }

    private async refreshExistedProfileNames(vc: Context) {
        const allExistingProfileNames = await UiProfileStore.fetchExistingProfileNames(vc);
        await this.metaStore.upsert(UiProfileStore.storeType, { existingProfileNames: allExistingProfileNames });
    }

    public async clearProfiles() {
        await this.store.clear();
    }

    private async initWithData(vc: Context) {
        const storeMeta = this.getStoreMeta();

        if (!storeMeta || !storeMeta.existingProfileNames || storeMeta.lastRefreshed + (this.namesInvalidationInterval) <= Date.now()) {
            await this.store.clear(); // There should not normally exist any entries.
            await this.refreshExistedProfileNames(vc);
        }

        if (!storeMeta || !storeMeta.profilesLastRefreshed || storeMeta.profilesLastRefreshed + (this.profilesInvalidationInterval) <= Date.now()) {
            await this.clearProfiles();
            await this.metaStore.upsert(UiProfileStore.storeType, { profilesLastRefreshed: Date.now(), lastProfilesUpdateChecked: Date.now() });
        }
    }

    public static async init({ dbName, redisClient, metaStore, chStore, vc, namesInvalidationInterval, profilesInvalidationInterval, profilesUpdateCheckInterval }: {
        dbName: string, redisClient?: any, metaStore: MetaStore, chStore: ClassHierarchyStore, vc: Context,
        namesInvalidationInterval?: number, profilesInvalidationInterval?: number, profilesUpdateCheckInterval?: number,
    }): Promise<UiProfileStore> {
        const store = await getStore({ dbName, name: UiProfileStore.storeType, redisClient });
        const upStore = new UiProfileStore({
            store, metaStore, chStore, namesInvalidationInterval, profilesInvalidationInterval, profilesUpdateCheckInterval,
        });
        await upStore.initWithData(vc);
        return upStore;
    }

    private static getInheritanceAncestorName(className: string, existingProfileNames: string[], inheritanceBranch: string[]): string | undefined {
        let matchedAncestor: string | undefined;
        inheritanceBranch.some((ancestorName: string) => {
            const exists = existingProfileNames.includes(ancestorName);
            if (exists) {
                matchedAncestor = ancestorName;
                return true;
            }
            return false;
        });
        return matchedAncestor;
    }

    private async getResolvedClassNamesMap(classNames: string[], vc?: Context): Promise<{[cn: string]: string | undefined}> {
        const existingProfileNames: string[] = this.getExistingProfileNames();
        const resolvedClassNamesMap: { [cn: string]: string | undefined } = {};
        const nonExistingProfileNames: string[] = [];

        classNames.forEach((className) => {
            if (existingProfileNames.includes(className)) {
                resolvedClassNamesMap[className] = className;
                return;
            }
            nonExistingProfileNames.push(className);
        });
        const hierarchyMap = await this.chStore.getMany(nonExistingProfileNames, vc);

        nonExistingProfileNames.forEach((className) => {
            resolvedClassNamesMap[className] = undefined;
            const classRDef = hierarchyMap[className];
            if (!classRDef) return;

            const { inheritanceBranch } = classRDef;
            resolvedClassNamesMap[className] = UiProfileStore.getInheritanceAncestorName(className, existingProfileNames, inheritanceBranch);
        });

        return resolvedClassNamesMap;
    }

    private async syncProfiles(classNames: string[], vc: Context, customFilter?: IPlainObject) {
        try {
            const newProfilesMap = await UiProfileStore.fetchUiProfiles(vc, classNames, customFilter);
            await this.store.import(Object.keys(newProfilesMap).map((className) => [className, newProfilesMap[className]]));
            return newProfilesMap;
        } catch (error) {
            return {};
        }
    }

    private async loadProfiles(classNames: string[], vc?: Context) {
        // TODO should use in-memory-cache first
        // console.log({ classNames });
        const profilesMap: IUiProfilesMap = entriesToMap(await this.store.entries(classNames));
        // console.log({ profilesMap, vc });
        if (!vc) return profilesMap;
        const nonFetchedProfileNames = classNames.filter((className) => !profilesMap[className]);

        // console.log({ nonCachedProfileNames });
        if (!nonFetchedProfileNames.length) return profilesMap;

        const newProfilesMap = await this.syncProfiles(nonFetchedProfileNames, vc);
        // console.log({ newProfilesMap });
        return Object.assign(profilesMap, newProfilesMap);
    }

    private async checkProfileUpdates(vc: Context) {
        const storeMeta: IPlainObject = this.getStoreMeta() || {};
        const lastUpdateChecked = storeMeta.lastProfilesUpdateChecked || 0;
        if (lastUpdateChecked + this.profilesUpdateCheckInterval >= Date.now()) return;

        const cachedProfileNames = await this.store.keys();
        if (!cachedProfileNames.length) return;
        await this.syncProfiles(cachedProfileNames, vc, { modified: { $gt: msToDate(lastUpdateChecked) } });
        await this.metaStore.upsert(UiProfileStore.storeType, { lastProfilesUpdateChecked: Date.now() });
    }

    public async getMany(classNames: string[], vc?: Context) {
        // step0: perform update checks
        if (vc) {
            this.checkProfileUpdates(vc); // no need to await.
        }
        // step1: Should resolve what to serve for each className
        const resolvedClassNamesMap = await this.getResolvedClassNamesMap(classNames, vc);
        // console.log({ resolvedClassNamesMap });
        const uiProfilesMap = await this.loadProfiles(Object.keys(resolvedClassNamesMap).map((k) => resolvedClassNamesMap[k]).filter((n) => !!n) as string[], vc);
        // console.log({ uiProfilesMap });
        const resolvedUiProfilesMap: IUiCompiledProfilesMap = {};
        classNames.forEach((className) => {
            const resolvedClass = resolvedClassNamesMap[className];
            if (!resolvedClass) return;
            const resolvedProfile = uiProfilesMap[resolvedClass];
            if (!resolvedProfile) return;
            resolvedUiProfilesMap[className] = new UiCompiliedProfileTemplate({
                uiProfile: deserializeUiProffileTemplates(resolvedProfile),
            });
        });
        return resolvedUiProfilesMap;
    }
}
