// TODO should properly handle errors at appropriate levels

import { OID } from '@vedavaapi/types/dist/JsonObject';
import { objstore, Context } from '@vedavaapi/client';
// @ts-ignore
import dset from 'dset';
import delve from 'dlv';
import Emitter from 'component-emitter';

import { IStore } from '@vedavaapi/iso-kvstores/dist/IStore';

import { IGraph } from '@vedavaapi/client/dist/api/objstore/response-models';
import { entriesToMap } from '../utils';
import { getStore } from '../IsoStore';
import { MetaStore } from './meta-store';


export interface IClassHierarchyItem {
    _id: OID;
    name: string;
    inheritanceBranch: string[];
    subClasses: string[];
}

export interface IClassHierarchy {
    [jsonClass: string]: IClassHierarchyItem;
}

export interface IClassDef {
    _id: OID;
    name: string;
    source?: string;
    _reached_ids?: string[];
}

/**
 * Loads entire hierarchy into cache on init, and then manages it to be in sync with upstream.
 * It stores only compiled truths, and serves them.
 */
export class ClassHierarchyStore extends Emitter {
    public static readonly storeType = 'ClassHierarchy';

    public static readonly NEW_CLASSES_ADDED = 'new_classes_added';

    private store: IStore;

    private metaStore: MetaStore;

    private cacheInvalidationInterval: number;

    constructor({ store, metaStore, cacheInvalidationInterval }: { store: IStore, metaStore: MetaStore, cacheInvalidationInterval?: number }) {
        super();
        this.store = store;
        this.metaStore = metaStore;
        this.cacheInvalidationInterval = cacheInvalidationInterval !== undefined ? cacheInvalidationInterval : 86400 * 1000;
    }

    private static async fetchRawHierarchy(vc: Context, startClass: string | string[], direction: 'referrer' | 'referred' = 'referrer', maxHops = -1) {
        const startNodesSelector = {
            jsonClass: 'VVSchema',
            name: Array.isArray(startClass) ? { $in: startClass } : startClass,
        };
        const resp = await objstore.Graph.put({
            vc,
            data: {
                start_nodes_selector: startNodesSelector,
                max_hops: maxHops,
                traverse_key_filter_maps_list: [{
                    source: { jsonClass: 'VVSchema' },
                }],
                json_class_projection_map: {
                    VVSchema: {
                        name: 1,
                        // _id: 1,
                        source: 1,
                        _reached_ids: 1,
                    },
                },
                // @ts-ignore
                direction,
            },
        });
        return resp.data;
    }

    private async loadEntriesMap(jsonClasses?: string[]): Promise<IClassHierarchy> {
        return entriesToMap(
            await this.store.entries(jsonClasses),
        );
    }

    private static computeAndSetInheritanceBranches(hUpdate: IClassHierarchy, hierarchy: IClassHierarchy) {
        const computedClasses: string[] = [];
        const _computeAndSet = (chItem: IClassHierarchyItem): string[] => {
            if (computedClasses.includes(chItem.name)) return chItem.inheritanceBranch;
            computedClasses.push(chItem.name);
            if (!chItem.inheritanceBranch.length) return chItem.inheritanceBranch;
            const parentName = chItem.inheritanceBranch[0];
            let parentBranch: string[] = [];
            if (hUpdate[parentName]) {
                parentBranch = _computeAndSet(hUpdate[parentName]);
            } else if (hierarchy[parentName]) {
                parentBranch = hierarchy[parentName].inheritanceBranch;
            }
            // eslint-disable-next-line no-param-reassign
            chItem.inheritanceBranch = [parentName, ...parentBranch];
            return chItem.inheritanceBranch;
        };

        Object.keys(hUpdate).forEach((name) => {
            _computeAndSet(hUpdate[name]);
        });
    }

    private emitNewClassesAddedEvent(newClassNames: string[]) {
        if (newClassNames.length) this.emit(ClassHierarchyStore.NEW_CLASSES_ADDED, newClassNames);
    }

    /**
     * Following runs only when, merges being happened.
     * if direction is referrer, It may be top down tree.
     *  else it should be down-top single branch
     */
    private async mergeClassesGraphResponse(graph: IGraph, startNodeIds: string[], direction = 'referrer') {
        const hierarchy = await this.loadEntriesMap();
        const hUpdate: IClassHierarchy = {};

        // We will first normalize __reached_ids to referrer direection
        if (direction !== 'referrer') {
            if (!startNodeIds.length) return {};
            const setReachedIds = (resId: string, childId?: string) => {
                const res = graph[resId];
                if (!res) return;
                if (!childId) {
                    dset(res, '_reached_ids.source', []);
                } else {
                    dset(res, '_reached_ids.source.0', childId);
                }
                const parentId: string | undefined = res.source as string | undefined;
                if (!parentId) return;
                setReachedIds(parentId, resId);
            };
            startNodeIds.forEach((snId) => setReachedIds(snId));
        }

        // now we will merge to hierarchy. We are currently using flat loop. We may also check recursion model
        Object.keys(graph).forEach((_id) => {
            const clsdef = graph[_id];
            const clsname = clsdef.name;
            const update: Partial<IClassHierarchyItem> = { name: clsname, _id };
            update.inheritanceBranch = [];
            if (clsdef.source) {
                const parentClsdef = graph[clsdef.source as string];
                if (parentClsdef) update.inheritanceBranch.push(parentClsdef.name);
            }
            const subClassIds: string[] = delve(clsdef, '_reached_ids.source', []);
            update.subClasses = subClassIds.map((scId) => delve(graph, [scId, 'name'], undefined)).filter((name) => !!name);

            const existingHItem = hierarchy[clsname];
            if (existingHItem) {
                update.subClasses = [...new Set([...existingHItem.subClasses, ...update.subClasses])];
            }
            hUpdate[update.name as string] = update as IClassHierarchyItem;
        });

        ClassHierarchyStore.computeAndSetInheritanceBranches(hUpdate, hierarchy);

        await this.store.import(
            Object.keys(hUpdate).map((name) => [name, hUpdate[name]]),
        );
        const newClassRDefs: IClassHierarchy = {};
        Object.keys(hUpdate).forEach((className) => {
            if (!hierarchy[className]) {
                newClassRDefs[className] = hUpdate[className];
            }
        });
        return newClassRDefs;
    }

    public async refresh(vc: Context) {
        // console.log('in refresh');
        const classGResp = await ClassHierarchyStore.fetchRawHierarchy(vc, 'JsonObject');
        // console.log({ classGResp });
        if (classGResp.start_nodes_ids.length) {
            await this.mergeClassesGraphResponse(classGResp.graph, classGResp.start_nodes_ids); // TODO should fire new classes added event?
        }
    }

    private async initWithData(vc: Context) {
        const storeMeta = this.metaStore.get(ClassHierarchyStore.storeType);
        // console.log({ storeMeta });

        if (!storeMeta || !storeMeta.initialized || storeMeta.lastRefreshed + (this.cacheInvalidationInterval) <= Date.now()) {
            await this.store.clear(); // There should not normally exist any entries.
            await this.refresh(vc);
            await this.metaStore.upsert(ClassHierarchyStore.storeType, { initialized: true }); // NOTE: registered after data has been fetched
        }
    }

    public static async init({ dbName, redisClient, metaStore, vc, cacheInvalidationInterval }: { dbName:string, redisClient?: any, metaStore: MetaStore, vc: Context, cacheInvalidationInterval?: number }): Promise<ClassHierarchyStore> {
        const store = await getStore({ dbName, name: ClassHierarchyStore.storeType, redisClient });
        // console.log({ store });
        const chStore = new ClassHierarchyStore({ store, metaStore, cacheInvalidationInterval });
        await chStore.initWithData(vc);
        return chStore;
    }

    public async names(): Promise<String[]> {
        return this.store.keys();
    }

    private async fetchAndSaveClasses(classNames: string[], vc: Context) {
        try {
            const cgResp = await ClassHierarchyStore.fetchRawHierarchy(vc, classNames, 'referred', -1);
            const newClassRdefs = await this.mergeClassesGraphResponse(cgResp.graph, cgResp.start_nodes_ids, 'referred');
            this.emitNewClassesAddedEvent(Object.keys(newClassRdefs));
            return newClassRdefs;
        } catch (error) {
            return {};
        }
    }

    public async getMany(classNames: string[], vc?: Context) {
        const entries = await this.store.entries(classNames);
        if ((entries.length === classNames.length) || !vc) return entriesToMap(entries) as IClassHierarchy;
        const existingNames = entries.map(([k]) => k);
        const nonExistingNames = classNames.filter((n) => !existingNames.includes(n));
        const newEntriesMap = await this.fetchAndSaveClasses(nonExistingNames, vc);
        return Object.assign(entriesToMap(entries), newEntriesMap);
    }


    public async get(jsonClass: string, vc?: Context): Promise<IClassHierarchyItem | undefined> {
        // TODO: test lazy check fnality
        const entriesMap = await this.getMany([jsonClass], vc);
        return entriesMap[jsonClass];
    }
}
