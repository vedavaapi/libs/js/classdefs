import { IStore } from '@vedavaapi/iso-kvstores/dist/IStore';
import { entriesToMap } from '../utils';
import { getStore } from '../IsoStore';


export interface IStoreMeta {
    lastRefreshed: number;
    lastUpdated?: number;
    [x: string]: any;
}

export interface IStoreMetaMap {
    [storeType: string]: IStoreMeta;
}

/**
 * this meta store just stores metadata about a Store.
 * It provides synchronous in memory app. iso-store is only to persist and retrieve that map.
 */
export class MetaStore {
    public static readonly storeType = 'MetaStore';

    private store: IStore;

    public metaMap: IStoreMetaMap;

    constructor(store: IStore) {
        this.store = store;
        this.metaMap = {};
    }

    private async loadMapFromStore() {
        const entries = await this.store.entries();
        this.metaMap = entriesToMap(entries);
    }

    public static async init({ dbName, redisClient }: { dbName: string, redisClient?: any }): Promise<MetaStore> {
        const store = await getStore({ dbName, name: MetaStore.storeType, redisClient });
        const metaStore = new MetaStore(store);
        await metaStore.loadMapFromStore();
        return metaStore;
    }

    /**
     * get is conciously synchronous
     */
    public get(storeType: string): IStoreMeta | undefined {
        return this.metaMap[storeType] ? JSON.parse(JSON.stringify(this.metaMap[storeType])) : undefined;
    }

    public async upsert(storeType: string, storeMeta: Partial<IStoreMeta>): Promise<IStoreMeta> {
        const meta: IStoreMeta = this.get(storeType) || {} as IStoreMeta;
        meta.lastRefreshed = Date.now();
        Object.assign(meta, storeMeta);
        meta.lastUpdated = Date.now();
        await this.store.set(storeType, meta);
        this.metaMap[storeType] = JSON.parse(JSON.stringify(meta));
        return meta;
    }

    public async delete(storeType: string): Promise<void> {
        if (!Object.prototype.hasOwnProperty.call(this.metaMap, storeType)) {
            return;
        }
        await this.store.delete(storeType);
        delete this.metaMap[storeType];
    }
}
