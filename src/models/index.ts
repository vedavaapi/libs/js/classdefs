import { ResourceReprTemplate } from './repr';
import { UiActionTemplate } from './actions';
import { IUiNavSpecsClassified, IUiSearchProfile, IUiProfile } from '../types';


export class UiCompiliedProfileTemplate {
    public repr: ResourceReprTemplate;

    public nav: IUiNavSpecsClassified;

    public actions: UiActionTemplate[];

    public search: IUiSearchProfile;

    constructor({ uiProfile }: {
        uiProfile: IUiProfile;
    }) {
        this.repr = new ResourceReprTemplate({ repr: uiProfile.repr || {} });
        this.nav = uiProfile.nav || {};
        this.actions = (uiProfile.actions || []).map((action) => new UiActionTemplate({ actionProfile: action,
        }));
        this.search = uiProfile.search || {};
    }
}
