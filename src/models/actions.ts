import { IPlainObject } from '@vedavaapi/types/dist/base';

import {
    // IUiAction,
    IUiActionParams, IDotTemplateData, IFieldTemplate, IUiAction, ILocaleTextOptions,
} from '../types';
import { computeFTValue } from '../utils';


export class ActionParamsTemplate {
    private _paramFTs: IUiActionParams;

    constructor({ paramsFTs }: { paramsFTs: IUiActionParams }) {
        this._paramFTs = paramsFTs;
    }

    public compute(field: string, dotData: IDotTemplateData): any {
        const ft = this._paramFTs[field] as IFieldTemplate | null;
        if (!ft) return null;
        return computeFTValue(ft, dotData);
    }

    public computeAll(dotData: IDotTemplateData): IPlainObject {
        const params: IPlainObject = {};
        Object.keys(this._paramFTs).forEach((field: string) => {
            params[field] = this.compute(field, dotData);
        });
        return params;
    }
}


export class UiActionTemplate {
    // private _actionProfile: IUiAction;

    public id: string;

    public label?: ILocaleTextOptions;

    public icon?: string;

    public iconType?: 'url' | 'iconClass' | 'svg';

    private _paramFTs: IUiActionParams;

    constructor({ actionProfile }: {
        actionProfile: IUiAction;
    }) {
        this._paramFTs = actionProfile.params || {};
        this.id = actionProfile.id;
        this.label = actionProfile.label;
        this.icon = actionProfile.icon;
        this.iconType = actionProfile.iconType;
    }

    public params(data: IDotTemplateData) : IPlainObject {
        return new ActionParamsTemplate({ paramsFTs: this._paramFTs }).computeAll(data);
    }
}
