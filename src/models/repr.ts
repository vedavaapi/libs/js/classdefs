import { IPlainObject } from '@vedavaapi/types/dist/base';
import { OID } from '@vedavaapi/types/dist/JsonObject';
import { IResource } from '@vedavaapi/types/dist/Resource';
import { IText } from '@vedavaapi/types/dist/Text';
import { IStillImageRepresentation } from '@vedavaapi/types/dist/StillImageRepresentation';
import { IFragmentSelector } from '@vedavaapi/types/dist/FragmentSelector';
import { ISelector } from '@vedavaapi/types/dist/Selector';
import { IProjection } from '@vedavaapi/client/dist/api/api-param-models';

import {
    IUiRepr,
    IUiReprCls,
    IUiReprInstance,
    IUiReprInstanceField,
    IResourceResolvedRepr,
    IDotTemplateData,
    IFieldTemplate,
    ILocaleTextOptions,
} from '../types';
import { computeFTValue } from '../utils';


export class ResourceInstanceReprTemplate {
    private _instanceRepr: IUiReprInstance;

    constructor({ instanceRepr }: { instanceRepr: IUiReprInstance }) {
        this._instanceRepr = instanceRepr;
    }

    public compute(field: IUiReprInstanceField, dotData: IDotTemplateData): any {
        const ft = this._instanceRepr[field] as IFieldTemplate;
        return computeFTValue(ft, dotData);
    }
}


export class ResourceResolvedRepr implements IResourceResolvedRepr {
    private _template: ResourceInstanceReprTemplate;

    private _data: IDotTemplateData;

    public _clsProfile: IUiReprCls;

    private _cache: IPlainObject;

    public _id: OID;

    public jsonClass: string;

    public jsonClassLabel?: string;

    public raw: IResource

    constructor({ template, data, clsProfile }: {
        template: ResourceInstanceReprTemplate;
        data: IDotTemplateData;
        clsProfile?: IUiReprCls;
    }) {
        this._template = template;
        this._data = data;
        this._clsProfile = clsProfile || {};
        this._cache = {};

        this._id = data._._id as OID;
        this.jsonClass = data._.jsonClass;
        this.jsonClassLabel = data._.jsonClassLabel;
        this.raw = data._;
    }

    private _memorizedGet(field: IUiReprInstanceField): any {
        if (!Object.prototype.hasOwnProperty.call(this._cache, field)) {
            this._cache[field] = this._template.compute(field, this._data);
        }
        const val = this._cache[field];
        return val !== null ? val : undefined;
    }

    public get name() : ILocaleTextOptions | string {
        return this._memorizedGet('name');
    }

    public get by(): string[] | undefined {
        return this._memorizedGet('by');
    }

    public get description(): ILocaleTextOptions | string {
        return this._memorizedGet('description');
    }

    public get img_repr(): IStillImageRepresentation | undefined {
        return this._memorizedGet('img_repr');
    }

    public get img_repr_selector(): IFragmentSelector | undefined {
        return this._memorizedGet('img_repr_selector');
    }

    public get txt_repr(): IText | string | undefined {
        return this._memorizedGet('txt_repr');
    }

    public get txt_repr_selector(): ISelector {
        return this._memorizedGet('txt_repr_selector');
    }
}


export class ResourceReprTemplate {
    // private _repr: IUiRepr;

    public cls: IUiReprCls; // cls profile is no template, but static value. hence no computation required either.

    public projection?: IProjection

    private _instanceReprTemplate: ResourceInstanceReprTemplate;

    constructor({ repr }: {
        repr: IUiRepr;
    }) {
        // this._repr = repr;
        this.cls = repr.cls || {};
        this.projection = repr.projection;
        this._instanceReprTemplate = new ResourceInstanceReprTemplate({ instanceRepr: repr.instance || {} });
    }

    public instance(data: IDotTemplateData) {
        return new ResourceResolvedRepr({ template: this._instanceReprTemplate, data, clsProfile: this.cls });
    }
}
