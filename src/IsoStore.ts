import { IDBStore } from '@vedavaapi/iso-kvstores/dist/idb-store';
import { IStore } from '@vedavaapi/iso-kvstores/dist/IStore';

import { isBrowser } from './utils';

export interface IStoreCls {
    new(optioons: any): IStore,
    init: (options: any) => Promise<IStore>;
}

let isoStore = IDBStore as unknown as IStoreCls;

if (!isBrowser) {
    // eslint-disable-next-line global-require
    isoStore = require('@vedavaapi/iso-kvstores/dist/redis-store').RedisStore as unknown as IStoreCls;
}

export const IsoStore = isoStore;


export async function getStore({ dbName, name, redisClient }: { dbName: string, name: string, redisClient?: any }): Promise<IStore> {
    let store!: IStore;
    if (isBrowser) {
        store = await IsoStore.init({
            databaseName: `${dbName}-${name}`, // NOTE
            name,
        });
    } else {
        store = await IsoStore.init({
            databaseName: dbName,
            name: `${dbName}-${name}`,
            client: redisClient,
        });
    }
    return store;
}
