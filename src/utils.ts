
import delve from 'dlv';
// @ts-ignore
import dot from 'dot/doT';
import { IPlainObject } from '@vedavaapi/types/dist/base';

import { IUiProfile, IUiReprInstance, IFTSpecifier, IUiReprInstanceField, IFieldTemplate, IDotTemplateData, ICompiledTemplate } from './types';


// eslint-disable-next-line no-new-func
export const isBrowser = new Function('try {return this===window;}catch(e){ return false;}')();

export const entriesToMap = (tuples: [string, any][]): IPlainObject => {
    const map: IPlainObject = {};
    tuples.forEach(([k, v]) => {
        map[k] = v as any;
    });
    return map;
};


export function pad(n: number, width: number, z = '0') {
    const num = `${n}`;
    return num.length >= width ? num : new Array(width - num.length + 1).join(z) + num;
}


export function msToDate(ms: number): string {
    const d = new Date(ms);
    return `${d.getUTCFullYear()}-${pad(d.getUTCMonth() + 1, 2)}-${pad(d.getUTCDate(), 2)} ${pad(d.getUTCHours(), 2)}:${pad(d.getUTCMinutes(), 2)}:${pad(d.getUTCSeconds(), 2)}.${pad(d.getMilliseconds(), 4)}`;
}


export function normalizeAndCompileFTChoice(ftChoice?: IFTSpecifier): IFieldTemplate | undefined {
    if (!ftChoice) return undefined;
    if (typeof ftChoice === 'string') { // means it's a field
        return { type: 'field', value: ftChoice };
    }
    if (typeof ftChoice === 'object') {
        if (ftChoice.type === 'template') {
            try {
                const compiledFn = dot.compile(ftChoice.value as string);
                return { type: 'template', value: `return ${compiledFn.toString()}` };
            } catch (error) {
                return undefined;
            }
        } else {
            return { ...ftChoice };
        }
    }
    return undefined;
}


function deSerializeCompiledFnString(ftChoice?: IFTSpecifier): IFTSpecifier | undefined {
    if (!ftChoice) return undefined;
    if (typeof ftChoice === 'string') return ftChoice;
    if (ftChoice.type === 'field') return ftChoice;
    try {
        // eslint-disable-next-line
        ftChoice.value = new Function(ftChoice.value as string)();
        return ftChoice;
    } catch (error) {
        return undefined;
    }
}


export function applyOnUiProfileTemplates(uiProfile: IUiProfile, middleWare: (ftChoice: IFTSpecifier) => IFTSpecifier | undefined): IUiProfile {
    const reprInstanceSpec: IUiReprInstance | undefined = delve(uiProfile, 'repr.instance', undefined);
    if (reprInstanceSpec) {
        Object.keys(reprInstanceSpec).forEach((rField: string) => {
            const ftChoice = reprInstanceSpec[rField as IUiReprInstanceField] as IFTSpecifier;
            reprInstanceSpec[rField as IUiReprInstanceField] = middleWare(ftChoice);
        });
    }

    if (!uiProfile.actions || !uiProfile.actions.length) return uiProfile;

    uiProfile.actions.forEach((action) => {
        const paramsMap = action.params;
        if (!paramsMap) return;
        Object.keys(paramsMap).forEach((param) => {
            const ftChoice = (paramsMap)[param];
            const result = middleWare(ftChoice);
            if (result) {
                paramsMap[param] = result;
            } else {
                delete paramsMap[param];
            }
        });
    });

    return uiProfile;
}


export function compileAndSerializeUiProfileTemplates(uiProfile: IUiProfile): IUiProfile {
    return applyOnUiProfileTemplates(uiProfile, normalizeAndCompileFTChoice);
}

export function deserializeUiProffileTemplates(uiProfile: IUiProfile): IUiProfile {
    return applyOnUiProfileTemplates(uiProfile, deSerializeCompiledFnString);
}


export function computeFTValue(ft: IFieldTemplate, data: IDotTemplateData): any {
    if (!ft) return null;
    try {
        if (ft.type === 'field') {
            const fieldOf = ft.field_of || '_';
            return delve(data, `${fieldOf}.${ft.value as string}`, null);
        }
        return (ft.value as ICompiledTemplate)(data);
    } catch (error) {
        return null;
    }
}
