import { IText } from '@vedavaapi/types/dist/Text';
import { IStillImageRepresentation } from '@vedavaapi/types/dist/StillImageRepresentation';
import { IFragmentSelector } from '@vedavaapi/types/dist/FragmentSelector';
import { ISelector } from '@vedavaapi/types/dist/Selector';
import { IResource } from '@vedavaapi/types/dist/Resource';
import { OID } from '@vedavaapi/types/dist/JsonObject';
import { IPlainObject } from '@vedavaapi/types/dist/base';


export type ILocaleTextOptions = Array<IText>

export type IFTType = 'field' | 'template';

export type IFieldContext = '_' | '_m' | 'p' | 'pm' | 'pr';

export type ICompiledTemplate = (dotData: IDotTemplateData) => string;

export interface IFieldTemplate {
    type?: IFTType,
    value: string | ICompiledTemplate;
    field_of?: IFieldContext,
}

export type IFTSpecifier = string | IFieldTemplate;


export type IUiReprClsField = 'label' | 'label_plural';

export type IUiReprCls = {
    [f in IUiReprClsField]?: ILocaleTextOptions;
};

export type IUiReprInstanceField = 'name' | 'by' | 'description' | 'img_repr' | 'img_repr_selector' | 'txt_repr' | 'txt_repr_selector'

export type IUiReprInstance = {
    [f in IUiReprInstanceField]?: IFTSpecifier;
}

export interface IUiReprProjection {
    [f: string]: 0 | 1;
}

export interface IUiRepr {
    cls?: IUiReprCls;
    instance?: IUiReprInstance;
    projection?: IUiReprProjection;
}


export interface IUiNavSpec {
    json_class: string;
    label?: ILocaleTextOptions;
    default_expanded?: boolean;
}

export type IUiNavLinkField = 'source' | 'target';

export type IUiNavSpecsClassified = {
    [f in IUiNavLinkField]?: IUiNavSpec[];
}


export interface IUiActionParams {
    [f: string]: IFTSpecifier;
}

export interface IUiAction {
    id: string;
    label?: ILocaleTextOptions;
    params?: IUiActionParams;
    icon?: string;
    iconType?: 'iconClass' | 'url' | 'svg';
}


export interface IUiSearchProfileSimple {
    fields: string[];
    label: ILocaleTextOptions;
}

export interface IUiSearchProfileAdvancedFieldSpec {
    field: string;
    label?: ILocaleTextOptions;
    required?: boolean;
    type?: 'string' | 'number' | '_id' | 'bool';
    allowedClasses?: string[]; // TODO enum support also?
}

export type IUiSearchProfileAdvanced = IUiSearchProfileAdvancedFieldSpec[];

export interface IUiSearchProfile {
    simple?: IUiSearchProfileSimple;
    advanced?: IUiSearchProfileAdvanced;
}


export enum UiProfileType {
    REPR = 'repr',
    NAV = 'nav',
    ACTIONS = 'actions',
    SEARCH = 'search',
}

export const UiProfileTypes = Object.keys(UiProfileType).map((upType) => UiProfileType[upType as keyof typeof UiProfileType]);

export interface IUiProfile {
    jsonClass?: 'UiProfile';
    [UiProfileType.REPR]?: IUiRepr;
    [UiProfileType.NAV]?: IUiNavSpecsClassified;
    [UiProfileType.ACTIONS]?: IUiAction[];
    [UiProfileType.SEARCH]?: IUiSearchProfile;
}


export interface IResourceResolvedRepr {
    readonly _id: OID;
    readonly jsonClass: string;
    readonly jsonClassLabel?: string;
    readonly name?: ILocaleTextOptions | string;
    readonly by?: string[];
    readonly description?: ILocaleTextOptions | string;
    readonly img_repr?: IStillImageRepresentation | string;
    readonly img_repr_selector?: IFragmentSelector;
    readonly txt_repr?: IText | string;
    readonly txt_repr_selector?: ISelector; // TODO should add support for TextQuoteSelector, etc.
    readonly cls?: IUiReprCls;
    readonly raw: IResource;
}


export interface IDotTemplateData {
    _: IResource;
    _m?: IPlainObject;
    p?: IResource;
    pm?: IPlainObject;
    pr?: IResourceResolvedRepr;
}
